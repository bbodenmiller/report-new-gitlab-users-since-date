#!/usr/bin/env python3

import argparse
import gitlab
from datetime import datetime
import csv
import os

# consider activity in the last 180 days

parser = argparse.ArgumentParser(description='Report new users since date')
parser.add_argument('gitlaburl', help='Url of the gitlab instance')
parser.add_argument('token', help='API token able to read the requested projects')
parser.add_argument('date', help='Date in YYYY-MM-DD')
args = parser.parse_args()

gitlaburl = args.gitlaburl if args.gitlaburl.endswith("/") else args.gitlaburl + "/"
gl = gitlab.Gitlab(gitlaburl, private_token=args.token)

reportdate = datetime.now().strftime('%Y-%m-%d')
since_date = args.date
if since_date > str(datetime.now().date()):
    print("Reliable predictions can only be made after the future has passed.")
    exit(1)

try:
    users = gl.users.list(as_list=False)
except Exception as e:
    print("Can not retrieve list of users: "+ str(e))
    exit(1)

signed_in = []
created_after = []
activity_after = []

for user in users:
    if user.attributes["state"] != "active":
        continue
    if "last_sign_in_at" not in user.attributes:
        print("Cannot access user field 'last_sign_in_at', use an admin token.")
        exit(1)
    else:
        if user.attributes["username"] not in ["ghost", "support-bot", "alert-bot"]:
            last_sign_in = user.attributes["last_sign_in_at"]
            last_activity = user.attributes["last_activity_on"]
            created_at = user.attributes["created_at"]
            if last_sign_in is not None and last_activity is not None and created_at is not None:
                last_sign_in_dt = datetime.strptime(last_sign_in, '%Y-%m-%dT%H:%M:%S.%f%z')
                created_at_dt = datetime.strptime(created_at, '%Y-%m-%dT%H:%M:%S.%f%z')
                last_activity_dt = datetime.strptime(last_activity, '%Y-%m-%d')
                if str(last_sign_in_dt.date()) > since_date:
                    signed_in.append(user.attributes)
                if str(last_activity_dt.date()) > since_date:
                    activity_after.append(user.attributes)
                if str(created_at_dt.date()) > since_date:
                    created_after.append(user.attributes)

created_report_path = "report/users_created_from_%s_to_%s.csv" % (since_date, str(datetime.now().date()))
os.makedirs(os.path.dirname(created_report_path), exist_ok=True)

with open(created_report_path, "w") as reportfile:
    reportwriter = csv.writer(reportfile, delimiter="\t", quotechar='"', quoting=csv.QUOTE_MINIMAL)
    fields = ["username","name","email","created_at"]
    reportwriter.writerow(fields)
    for user in created_after:
        row = []
        for field in fields:
            row.append(user[field])
        reportwriter.writerow(row)

signin_report_path = "report/users_signed_in_from_%s_to_%s.csv" % (since_date, str(datetime.now().date()))
os.makedirs(os.path.dirname(signin_report_path), exist_ok=True)

with open(signin_report_path, "w") as reportfile:
    reportwriter = csv.writer(reportfile, delimiter="\t", quotechar='"', quoting=csv.QUOTE_MINIMAL)
    fields = ["username","name","email","last_sign_in_at"]
    reportwriter.writerow(fields)
    for user in signed_in:
        row = []
        for field in fields:
            row.append(user[field])
        reportwriter.writerow(row)

activity_report_path = "report/users_activity_from_%s_to_%s.csv" % (since_date, str(datetime.now().date()))
os.makedirs(os.path.dirname(activity_report_path), exist_ok=True)

with open(activity_report_path, "w") as reportfile:
    reportwriter = csv.writer(reportfile, delimiter="\t", quotechar='"', quoting=csv.QUOTE_MINIMAL)
    fields = ["username","name","email","last_activity_on"]
    reportwriter.writerow(fields)
    for user in activity_after:
        row = []
        for field in fields:
            row.append(user[field])
        reportwriter.writerow(row)