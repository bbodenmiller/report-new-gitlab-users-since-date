# Report GitLab user activity since date

Provides a script to list all recently active, signed in and created users since a given date.

## Configuration

- Export / fork repository.
- Add a GitLab **admin** API token to CI/CD variables named `GITLAB_TOKEN`. Make sure it is "masked".
This token will be used to query the API for group and project members.
The token **must** be an admin token to be able to list all users and the dates in question.
- Add your GitLab instance URL to CI/CD variables named `GITLAB_URL`.
- Set the start date for the report in CI/CD variables as `SINCE_DATE`. Format is YYYY-MM-DD.
- notice the Job is tagged "local" for testing purposes. Make sure your runners pick it up.

## Usage

`python3 report-user-activity.py $GITLAB_URL $GITLAB_TOKEN $SINCE_DATE`

$SINCE_DATE format is YYYY-MM-DD as in 2020-04-29 for April 29th, 2020.

## What does it do?

* Request all users using https://docs.gitlab.com/ee/api/users.html#for-admins
* For all users:
  * is user as last_activity_on, created_at or last_sign_in_at between $SINCE_DATE and today, add to respective report
  * Write CSV reports of all users fitting the criteria
